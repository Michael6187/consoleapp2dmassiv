﻿#include <iostream>
#include <ctime>

using namespace std;
//
//int main()
//{
//    const int SIZE = 10;
//
//    int array[SIZE][SIZE] = { {5,6,1,8,3,7,4,2,9,10}, {10,9,8,7,6,5,4,3,2,1} };
//
//    for (int i = 0; i < SIZE; i++)
//    {
//        for (int j = 0; j < SIZE; j++)
//        {
//            cout << array[i][j];
//        }
//        cout << "\n";
//    }
//}
int main()
{
	setlocale(LC_ALL, "ru");

	const int n = 6;
	int array[n][n] = { {5,6,8,3,7,9},{4,7,1,4,7,9},{4,7,1,1,5,3},{9,4,1,2,3},{5,7,2,1,7,3},{3,8,6,9,5,4} };

	for (int i = 0; i < n; i++)
	{
		int sum = 0;

		time_t t = time(0);
		tm Localtime;
		localtime_s(&Localtime, &t);

		for (int j = 0; j < n; j++)
		{
			cout << array[i][j];

			//сумма строки
			sum =sum + array[i][j];
		}

		//24 % n == i
		if ((&Localtime)->tm_mday % n==i)
		{
			cout << "\t" << sum << "\n";
		}
		else
		{
			cout <<" Индекс не равен остатку деления на N"<< "\n";
		}
	}
}